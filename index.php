    <?php include 'layout/header.php';
    session_start();
    if (!empty($_GET['action']) && file_exists('views/'. $_GET['action'] .'.php')) {
        include 'views/'. $_GET['action'] .'.php';
    } else {
        include 'views/main.php';
    }