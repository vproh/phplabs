export default class ArticleSearch {
    constructor() {
        this.articleList = $('.post');
        this.searchInput = $('#search');
        this.emptyArticleListInfo = $('#emptyArticleListMessage');
    }

    initListeners() {
        this.searchInput.on('keyup', this.searchArticles.bind(this));
        this.showEmptyInfo();
    }

    showEmptyInfo() {
        let hiddenArticles = $(".post:hidden");

        if (this.isArticleListEmpty(hiddenArticles)) {
            this.emptyArticleListInfo.show();
        } else {
            this.emptyArticleListInfo.hide();
        }
    }

    searchArticles() {
        let searchRequest = this.searchInput.val();

        for (let i = 0; i < this.articleList.length; ++i) {
            let article = $(this.articleList[i]);
            let articleTitle = article.find('h1.my-4').text().toLowerCase();

            if (articleTitle.indexOf(searchRequest) === -1) {
                article.hide();
            } else {
                article.show();
            }
        }

        this.showEmptyInfo();
    }

    isArticleListEmpty(hiddenArticles) {
        return this.articleList.length === hiddenArticles.length;
    }
}