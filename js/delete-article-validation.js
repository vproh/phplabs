export default class DeleteValidation {
    constructor() {
        this.deleteButton = $('.delete-article');
        this.deleteSubmit = $('#deleteArticleSubmit');
        this.deleteArticleModal = $('#deleteArticleModal');
    }

    initListeners() {
        this.deleteButton.on("click", this.deleteArticleClicked.bind(this));
    }

    deleteArticleClicked(event) {
        let target = $(event.target);

        this.deleteSubmit.attr('href', 'index.php?action=delete_article&id=' + target.data('id'));
        console.log(this.deleteSubmit.attr('href'));
        console.log(target.data('id'));
        this.deleteArticleModal.modal();
    }
}