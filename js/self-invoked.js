import UserRegistration from "./user-registration.js";
import UserAuthorization from "./user-authorization.js";
import CreateArticle from "./create-article.js";
import DeleteValidation from "./delete-article-validation.js";
import ArticleSearch from "./article-search.js";

(function () {
    let userRegistration = new UserRegistration();
    let userAuthorization = new UserAuthorization();
    let createArticle = new CreateArticle();
    let deleteArticleValidator = new DeleteValidation();
    let articleSearch = new ArticleSearch();

    userRegistration.initListeners();
    userAuthorization.initListeners();
    createArticle.initListeners();
    deleteArticleValidator.initListeners();
    articleSearch.initListeners();
}());