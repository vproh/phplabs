export default class UserAuthorization {
    constructor() {
        this.loginInput = $("#login");
        this.passwordInput = $("#loginPassword");
        this.loginSubmitButton = $("#loginSubmitButton");
        this.loginForm = $("#loginForm");
        this.errorMessage = $("#credErrorMessage");
    }

    initListeners() {
        this.loginSubmitButton.on("click", this.loginAuthorizationClicked.bind(this));
    }

    loginAuthorizationClicked(event) {
        event.preventDefault();
        this.errorMessage.html("");
        if (this.validateForm()) {
            this.loginForm.submit();
        }
    }

    validateForm() {
        let isFormValid = true;

        let login = $.trim(this.loginInput.val());
        let loginErrorDiv = this.loginInput.siblings(".errorMessage");
        if (login === "") {
            isFormValid = false;
            loginErrorDiv.html("Login cannot be empty");
        } else {
            loginErrorDiv.html("");
        }

        let password = $.trim(this.passwordInput.val());
        let passwordErrorDiv = this.passwordInput.siblings(".errorMessage");
        if (password === "") {
            isFormValid = false;
            passwordErrorDiv.html("Password cannot be empty");
        } else {
            passwordErrorDiv.html("");
        }

        return isFormValid;
    }
}