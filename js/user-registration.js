export default class UserRegistration {
    constructor() {
        this.registrationForm = $("#registrationForm");
        this.loginInput = $("#registrationLogin");
        this.emailInput = $("#registrationEmail");
        this.addressInput = $("#registrationAddress");
        this.passwordInput = $("#registrationPassword");
        this.repeatPasswordInput = $("#registrationPasswordRepeat");
        this.submitButton = $("#registrationSubmitButton");
        this.dbErrorMessage = $("#dbErrorMessage");
    }

    initListeners() {
        this.submitButton.on("click", this.registerNewUser.bind(this));
    }

    registerNewUser(event) {
        event.preventDefault();
        this.dbErrorMessage.html("");
        if (this.validateForm()) {
            this.registrationForm.submit();
        }
    }

    validateForm() {
        let isValid = true;

        let loginErrorDiv = this.loginInput.siblings(".errorMessage");
        let login = $.trim(this.loginInput.val());
        let loginRegExp = new RegExp("^[a-zА-я\-_\\d]{4,20}$", "gmi");
        if (!loginRegExp.test(login)) {
            loginErrorDiv.html("Please enter valid login. Can contain A-zА-я_- at least 4 symbols");
            isValid = false;
        } else {
            loginErrorDiv.html("");
        }

        let emailErrorDiv = this.emailInput.siblings(".errorMessage");
        let email = $.trim(this.emailInput.val());
        let emailRegExp = new RegExp("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", "gm");
        if (!emailRegExp.test(email)) {
            emailErrorDiv.html("Please enter valid email. Must contain @ and .");
            isValid = false;
        } else {
            emailErrorDiv.html("");
        }

        let addressErrorDiv = this.addressInput.siblings(".errorMessage");
        let address = $.trim(this.addressInput.val());
        let addressRegExpUkr = new RegExp("^([їіА-я]{2,6}\\. [їіА-я ?\\-?]+, \\d{1,3}((\\\\\\d{1,3})|([A-z]))?){0,255}$", "gm");
        let addressRegExpEng = new RegExp("^([A-z ?\\-?]+ [A-z]{2,6}\\., \\d{1,3}((\\\\\\d{1,3})|([A-z]))?){0,255}$", "gm");
        if (!addressRegExpUkr.test(address) && !addressRegExpEng.test(address)) {
            addressErrorDiv.html("Please enter valid address, like вул. героїв Майдану, 4a or Geroiv Maidanu st., 1/11");
            isValid = false;
        } else {
            addressErrorDiv.html("");
        }

        let passwordErrorDiv = this.passwordInput.siblings(".errorMessage");
        let password = $.trim(this.passwordInput.val());
        let passwordRegExp = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{7,15}$", "gm");
        if (!passwordRegExp.test(password)) {
            passwordErrorDiv.html("Please enter valid password. Must contain one upper one lower case and digit at least 7 symbols");
            isValid = false;
        } else {
            passwordErrorDiv.html("");
        }

        let passwordRepeat = $.trim(this.repeatPasswordInput.val());
        let repeatErrorDiv = this.repeatPasswordInput.siblings(".errorMessage");
        if (password !== passwordRepeat) {
            repeatErrorDiv.html("Passwords must be equal");
            isValid = false;
        } else {
            repeatErrorDiv.html("");
        }

        return isValid;
    }
}