<div class="site-wrapper">
    <div class="site-wrapper-inner">
        <div class="container about">
            <div class="inner cover">
                <h1 class="cover-heading">Blog <span class="poster">Poster</span></h1>
                <p class="lead">
                    Good place where you can publish beautiful posts on nature thematic <br>
                    The site was made by Vlad Prokhnitskyi
                </p>
                <p class="lead">
                    <a href="index.php" class="btn btn-lg btn-default">Learn more</a>
                </p>
            </div>
        </div>
    </div>
</div>