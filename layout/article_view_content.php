<style>
    .header {
        background: #4DFAD2;
        width: 100%;
        background: linear-gradient(
            to bottom,
            rgba(77, 250, 210, 0.6),
            rgba(127, 250, 221, 0.5)
        ) ,url('<?= $articleImage; ?>') no-repeat left top;
        background-size: cover;
        z-index: 0;
    }
</style>
<div class="header">
    <div class="text-center">
        <div class="caption">
            <br><br><br>
            <h2 class="title display-3"><?= $articleTitle; ?></h2>
            <h3>
                Author: <?= $authorLogin; ?>
                <br><hr>
                <i class="fa fa-calendar"></i> Posted on: <?= $creationDate ?><br>
                <i class="fa fa-calendar"> Updated: <?= $modifyDate; ?> </i>
            </h3>
            <hr>
            <h4>Article about what:</h4><br>
            <h4><?= $articleDescription; ?></h4>
        </div>
    </div>
</div>

<div class="container w-75">
    <div class="card mb-4">
        <div class="card-body text-center">
            <h4><p><?= $articleContent; ?></p></h4>
        </div>
    </div>
</div>