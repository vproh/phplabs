<header>
    <nav class="navbar navbar-default navbar-expand-lg navbar-light">
        <div class="navbar-header d-flex col">
            <a class="navbar-brand" href="explore">Blog<b>Poster</b></a>
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle navbar-toggler ml-auto">
                <span class="navbar-toggler-icon"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="pull-right search">
                <form method="get" class="navbar-form form-inline">
                    <div class="input-group search-box">
                        <input type="search" name="postTitle" id="search" class="form-control" placeholder="Search some posts...">
                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    </div>
                </form>
            </div>
        </div>
        <div id="navbarCollapse" class="collapse navbar-collapse justify-content-start">
            <ul class="nav navbar-nav ml-auto">
                <li style="padding-left: 5.5rem; margin-left: -5.5rem;" class="nav-item dropdown pull-right">
                    <a data-toggle="dropdown" class="nav-link dropdown-toggle" href="#"><?= $_SESSION["userLogin"]; ?><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="index.php?action=create_article" class="dropdown-item">Create new article</a></li>
                        <li><a href="index.php?action=log_out" class="dropdown-item">Log out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>