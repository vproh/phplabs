<div class="container py-3 w-75">
    <div class="stepwizard text-center">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step">
                <a id="firstStep" href="#step-1" type="button" class="btn btn-circle">1</a>
                <p>Step 1</p>
            </div>
            <div class="stepwizard-step">
                <a href="#step-2" type="button" class="btn btn-circle disabled">2</a>
                <p>Step 2</p>
            </div>
            <div class="stepwizard-step">
                <a href="#step-3" type="button" class="btn btn-circle disabled">3</a>
                <p>Step 3</p>
            </div>
        </div>
    </div>
    <form role="form" action="" method="post" enctype="multipart/form-data" id="createArticleForm">
        <div class="setup-content" id="step-1">
            <h3>Step 1</h3>
            <div class="form-group">
                <label class="control-label">Post title</label>
                <input name="articleTitle" maxlength="255" type="text" required class="form-control"
                       placeholder="Yours title..."/>
            </div>
            <div class="form-group">
                <label class="control-label">Short description</label>
                <textarea style="height: 5rem" name="articleDescription" form="createArticleForm" maxlength="500"
                          type="text"
                          required class="form-control" placeholder="Start typing here..."></textarea>
            </div>

            <button class="btn btn-success nextBtn" type="button">Next</button>
            <a href="index.php" class="btn btn-danger">Cancel</a>

        </div>
        <div class="setup-content" id="step-2">
            <h3>Step 2</h3>
            <div class="form-group">
                <label class="control-label">Post content</label>
                <textarea style="height: 10rem;" name="articleContent" form="createArticleForm" type="text" required
                          class="form-control" placeholder="Start typing here..."></textarea>
            </div>
            <br>
            <button type="button" class="btn btn-success nextBtn">Next</button>
            <a href="index.php" class="btn btn-danger">Cancel</a>
        </div>
        <div class="setup-content" id="step-3">
            <h3>Step 3</h3>
            <div class="form-group">
                <label class="control-label">Choose a picture for your Post</label>
                <input required class="form-control-file" type="file" form="createArticleForm" name="articleImage"
                       id="image">
            </div>
            <br>
            <button id="createArticle" type="button" class="btn btn-success">Create</button>
            <a href="index.php" class="btn btn-danger">Cancel</a>
            <div id="submitError" class="errorMessage hidden">
                <?= $creationError; ?>
            </div>
        </div>
    </form>
</div>