<div class="container py-3 w-75">
    <form role="form" action="" method="post" enctype="multipart/form-data" id="editArticle">
        <div class="form-group">
            <label for="titleInput" class="control-label">Post title:</label>
            <input id="articleTitle" name="articleTitle" maxlength="255" type="text" required="required" class="form-control" value="<?= $articleTitle; ?>"/>
        </div><hr>
        <div class="form-group">
            <label for="descriptionText" class="control-label">Short description:</label>
            <textarea style="height: 5rem;" id="articleDescription" name="articleDescription" form="editArticle" type="text" required="required" class="form-control"><?= $articleDescription; ?></textarea>
        </div><hr>
        <div class="form-group">
            <label for="contentText" class="control-label">Post content:</label>
            <textarea style="height: 10rem;" id="contentText" name="articleContent" form="editArticle" type="text" required="required" class="form-control"><?= $articleContent; ?></textarea>
        </div><hr>
        <div class="form-group">
            <label class="control-label">Current post image:</label><br>
            <img class="card-img-top" src="<?= $articleImage ?>" alt="Something went wrong"><br>
            <label class="control-label">Choose new one:</label><br>
            <input class="form-control-file" type="file" name="articleImage" id="image">
        </div>
        <input class="btn btn-success" type="submit" value="Save">
        <a href="index.php?action=main" class="btn btn-danger">Cancel</a>
        <div class="errorMessage">
            <?= $error; ?>
        </div>
    </form>
</div>