<header>
    <div class="container-fluid">
        <nav class="navbar navbar-default navbar-expand-lg navbar-light">
            <div class="navbar-header d-flex col">
                <a class="navbar-brand" href="index.php">Blog<b>Poster</b></a>
            </div>
            <div class="pull-right auth-label">
                Authorization
            </div>
        </nav>
    </div>
</header>
<div class="container">
    <form action="" method="post" id="loginForm">
        <label>
            <p class="label-txt">ENTER YOUR LOGIN</p>
            <input id="login" name="login" type="text" value="<?= htmlspecialchars($login); ?>"
                   class="input">
            <div class="line-box">
                <div class="line-success"></div>
            </div>
            <div class="errorMessage">
                <?= $emptyLoginError; ?>
            </div>
        </label>
        <label>
            <p class="label-txt">ENTER YOUR PASSWORD</p>
            <input id="loginPassword" name="password" type="password" class="input">
            <div class="line-box">
                <div class="line-success"></div>
            </div>
            <div class="errorMessage">
                <?= $emptyPasswordError; ?>
            </div>
        </label>
        <button id="loginSubmitButton" name="loginSubmit" type="submit">Login</button>
        <div id="credErrorMessage" class="errorMessage">
            <?= $authorizationError; ?>
        </div>
        <div class="text-center"><br>
            Doesn`t have an account yet? <a href="index.php?action=registration">Register now</a>
        </div>
    </form>
</div>