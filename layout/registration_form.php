<header>
    <div class="container-fluid">
        <nav class="navbar navbar-default navbar-expand-lg navbar-light">
            <div class="navbar-header d-flex col">
                <a class="navbar-brand" href="index.php">Blog<b>Poster</b></a>
            </div>
            <div class="pull-right auth-label">
                Registration
            </div>
        </nav>
    </div>
</header>
<div class="container">
    <form action="" method="post" id="registrationForm">
        <label>
            <p class="label-txt">ENTER YOUR LOGIN</p>
            <input id="registrationLogin" name="login" type="text" value="<?= htmlspecialchars($login); ?>"
                   class="input">
            <div class="line-box">
                <div class="line-success"></div>
            </div>
            <div class="errorMessage">
                <?= $loginError; ?>
            </div>
        </label>
        <label>
            <p class="label-txt">ENTER YOUR EMAIL</p>
            <input id="registrationEmail" name="email" type="email" value="<?= htmlspecialchars($email); ?>"
                   class="input">
            <div class="line-box">
                <div class="line-success"></div>
            </div>
            <div class="errorMessage">
                <?= $emailError; ?>
            </div>
        </label>
        <label>
            <p class="label-txt">ENTER YOUR ADDRESS</p>
            <input id="registrationAddress" name="address" type="text" value="<?= htmlspecialchars($address); ?>"
                   class="input">
            <div class="line-box">
                <div class="line-success"></div>
            </div>
            <div class="errorMessage">
                <?= $addressError; ?>
            </div>
        </label>
        <label>
            <p class="label-txt">ENTER YOUR PASSWORD</p>
            <input id="registrationPassword" name="password" type="password" class="input">
            <div class="line-box">
                <div class="line-success"></div>
            </div>
            <div class="errorMessage">
                <?= $passwordError; ?>
            </div>
        </label>
        <label>
            <p class="label-txt">REPEAT YOUR PASSWORD</p>
            <input id="registrationPasswordRepeat" name="repeatPassword" type="password" class="input">
            <div class="line-box">
                <div class="line-success"></div>
            </div>
            <div class="errorMessage">
                <?= $repeatPasswordError; ?>
            </div>
        </label>
        <button id="registrationSubmitButton" name="registerSubmit" type="submit">Submit</button>
        <div id="dbErrorMessage" class="errorMessage">
            <?= $dbError; ?>
        </div>
        <div class="text-center"><br>
            Already have an account? <a href="index.php?action=authorization">Sign in</a>
        </div>
    </form>
</div>