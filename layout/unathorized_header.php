<header>
    <div class="container-fluid">
        <nav class="navbar navbar-default navbar-expand-lg navbar-light">
            <div class="navbar-header d-flex col">
                <a class="navbar-brand" href="index.php">Blog<b>Poster</b></a>
                <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle navbar-toggler ml-auto">
                    <span class="navbar-toggler-icon"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <div id="navbarCollapse" class="collapse navbar-collapse pull-right justify-content-start">
                <a href="index.php?action=authorization" class="btn btn-outline-primary">Login</a>
                <a href="index.php?action=registration" class="btn btn-success">Sign up</a>
            </div>
        </nav>
    </div>
</header>