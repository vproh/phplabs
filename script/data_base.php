<?php

function get_db_connection() {
    static $dataBase;

    if ($dataBase === NULL) {
        $dataBase = new mysqli("localhost", "root", "", "blog_poster_php");

        if ($dataBase->connect_errno) {
            die($dataBase->connect_error);
        }
    }

    $dataBase->set_charset("utf8");

    return $dataBase;
}