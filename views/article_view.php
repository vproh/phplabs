<?php
if (array_key_exists('userId', $_SESSION)) {
    include "layout/authorized_header.php";
} else {
    include "layout/unathorized_header.php";
}

include "script/data_base.php";

$articleId = !empty($_GET['id']) ? $_GET['id'] : 0;
if ($articleId == 0) {
    header('Location: index.php?action=main');
    die("Incorrect article id");
}

$articleData = get_article_data_by_id($articleId);
if ($articleData == null) {
    header('Location: index.php?action=main');
    die("No such article");
}

$articleVisible = $articleData['visible'];
if (!$articleVisible && array_key_exists('userAdmin', $_SESSION) ? $_SESSION["userAdmin"] : false) {
    header('Location: index.php?action=main');
    die("This article is denied");
}

$articleTitle = $articleData['title'];
$articleDescription = $articleData['description'];
$articleContent = $articleData['content'];
$articleImage = $articleData['image_url'];
$creationDate = $articleData['creation_date'];
$modifyDate = $articleData['modify_date'];
$authorLogin = get_author_login_by_id($articleData['author_id']);

function get_article_data_by_id($id)
{
    $dbConnection = get_db_connection();
    $statement = $dbConnection->prepare("SELECT title, description, content, image_url, creation_date, modify_date, visible, author_id FROM articles WHERE id = ?");
    if ($statement) {

        $statement->bind_param("i", $id);
        $statement->execute();
        $result = $statement->get_result();

        return $result->fetch_assoc();
    }

    return null;
}

function get_author_login_by_id($id)
{
    $dbConnection = get_db_connection();
    $statement = $dbConnection->prepare("SELECT login FROM users WHERE id = ?");

    if ($statement && !$dbConnection->errno) {
        $statement->bind_param("i", $id);
        $statement->execute();
        $result = $statement->get_result();
        $loginData = $result->fetch_assoc();

        return $loginData['login'];
    }

    return null;
}

include 'layout/article_view_content.php';
include 'layout/footer.php';