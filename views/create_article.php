<?php include "layout/authorized_header.php"; ?>

<?php
if (!array_key_exists('userId', $_SESSION)) {
    header('Location: index.php?action=main');
    die();
}

include "script/data_base.php";
include "script/upload_image.php";

$creationError = '';
$title = '';
$description = '';
$content = '';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $title = $_POST["articleTitle"];
    $description = $_POST["articleDescription"];
    $content = $_POST["articleContent"];
    $imagePath = upload_article_image();

    if (!$title || !$description || !$content || $imagePath == '') {
        $creationError = "There some error(s). Please review all steps and try again";
    } else {
        create_article($creationError, $title, $description, $content, $imagePath);
        header('Location: index.php?action=main');
    }
}

function create_article(&$creationError, $title, $description, $content, $imagePath)
{
    $dbConnection = get_db_connection();
    $userId = $_SESSION['userId'];
    $visible = $_SESSION['userAdmin'];

    $statement = $dbConnection->prepare("INSERT INTO articles (title, description, content, image_url, creation_date, modify_date, visible, author_id) 
    VALUES (?, ?, ?, ?, now(), now(), ?, ?)");

    if ($statement) {
        $statement->bind_param("ssssii", $title, $description, $content, $imagePath, $visible, $userId);
        $statement->execute();
    } elseif ($dbConnection->errno || !$statement) {
        $creationError = "Something went wrong, please try again later";

        return;
    }
}

include "layout/create_layout.php";
include "layout/footer.php";
?>
