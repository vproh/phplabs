<?php

if (!array_key_exists('userAdmin', $_SESSION) || !$_SESSION['userAdmin']) {
    header('Location: index.php?action=main');
    die();
}

include "script/data_base.php";

$articleId = !empty($_GET['id']) ? $_GET['id'] : 0;
if ($articleId == 0) {
    header('Location: index.php?action=main');
    die("Incorrect article id");
}

if (!is_article_visible($articleId) || array_key_exists('userAdmin', $_SESSION) ? !$_SESSION["userAdmin"] : false) {
    header('Location: index.php?action=main');
    die("Access denied");
}

    $dbConnection = get_db_connection();
    $statement = $dbConnection->prepare("DELETE FROM articles WHERE id = ?");

    if ($statement) {
        $statement->bind_param("i", $articleId);
        $statement->execute();
        header('Location: index.php?action=main');
    }

    function is_article_visible($articleId) {
        $dbConnection = get_db_connection();
        $statement = $dbConnection->prepare("SELECT visible FROM articles WHERE id = ?");

        if ($statement) {
            $statement->bind_param("i", $articleId);
            $statement->execute();
            $result = $statement->get_result();
            $loginData = $result->fetch_assoc();

            return $loginData['visible'];
        }

        return false;
    }