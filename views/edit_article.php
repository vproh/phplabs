<?php include "layout/authorized_header.php"; ?>

<?php
if (!array_key_exists('userAdmin', $_SESSION) || !$_SESSION['userAdmin']) {
    header('Location: index.php?action=main');
    die();
}

include "script/data_base.php";
include "script/upload_image.php";

$articleId = !empty($_GET['id']) ? $_GET['id'] : 0;
if ($articleId == 0) {
    header('Location: index.php?action=main');
    die("Incorrect article id");
}

$articleData = get_article_data_by_id($articleId);
if ($articleData == null) {
    header('Location: index.php?action=main');
    die("No such article");
}

$articleVisible = $articleData['visible'];
if (!$articleVisible && array_key_exists('userAdmin', $_SESSION) ? $_SESSION["userAdmin"] : false) {
    header('Location: index.php?action=main');
    die("This article is denied");
}

$articleTitle = $articleData['title'];
$articleDescription = $articleData['description'];
$articleContent = $articleData['content'];
$articleImage = $articleData['image_url'];
$authorLogin = get_author_login_by_id($articleData['author_id']);

$error = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    filter_post_input();
    $isValid = true;
    $updatedTitle = $_POST['articleTitle'];
    $updatedDescription = $_POST['articleDescription'];
    $updatedContent = $_POST['articleContent'];
    if (strlen($updatedTitle) == 0 || strlen($updatedDescription) == 0 || strlen($updatedContent) == 0) {
        $isValid = false;
        $error = "Some fields are empty";
    }

    if ($isValid) {
        $updatedImage = upload_article_image();
        update_article_by_id($updatedTitle, $updatedDescription, $updatedContent, strlen($updatedImage) == 0 ? $articleImage : $updatedImage, $articleId);
        header('Location: index.php?action=main');
    }
}

function get_article_data_by_id($id) {
    $dbConnection = get_db_connection();
    $statement = $dbConnection->prepare("SELECT title, description, content, image_url, visible, author_id FROM articles WHERE id = ?");
    if ($statement) {

        $statement->bind_param("i", $id);
        $statement->execute();
        $result = $statement->get_result();

        return $result->fetch_assoc();
    }

    return null;
}

function get_author_login_by_id($id) {
    $dbConnection = get_db_connection();
    $statement = $dbConnection->prepare("SELECT login FROM users WHERE id = ?");

    if ($statement && !$dbConnection->errno) {
        $statement->bind_param("i", $id);
        $statement->execute();
        $result = $statement->get_result();
        $loginData = $result->fetch_assoc();

        return $loginData['login'];
    }

    return null;
}

function filter_post_input()
{
    foreach ($_POST as $key => $value) {
        $values[$key] = trim(stripslashes($value)); // basic input filter
    }
}

function update_article_by_id($title, $articleDescription, $articleContent, $articleImage, $id) {
    $dbConnection = get_db_connection();
    $statement = $dbConnection->prepare("UPDATE articles SET title = ?, description = ?, content = ?, modify_date = now(), image_url = ? WHERE id = ?");

    if ($statement && !$dbConnection->errno) {
        $statement->bind_param("ssssi", $title, $articleDescription, $articleContent, $articleImage, $id);
        $statement->execute();
    }
}

include "layout/edit_layout.php";
include "layout/footer.php";
?>

