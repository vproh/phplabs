<?php
    if (array_key_exists('userId', $_SESSION)) {
        include "layout/authorized_header.php";
    } else {
        include "layout/unathorized_header.php";
    }

    include "script/data_base.php";
?>
<div class="container">

    <div id="emptyArticleListMessage" class="alert text-center alert-info hidden">
        There are no articles. But do not worry You can <br>
        <a type="button"  class="btn btn-success" href="index.php?action=create_article">
            Create new article
        </a>
    </div>

    <?php
    $error = "";
    $articlesData = get_articles();
    $isAdmin = array_key_exists('userAdmin', $_SESSION) ? $_SESSION["userAdmin"] : false;
    $listing = "";

    while($article = $articlesData->fetch_assoc()) {
        $authorLogin = get_author_login_by_id($error, $article['author_id']);
        $title = $article['title'];
        $description = $article['description'];
        $creationDate = $article['creation_date'];
        $articleId = $article['id'];
        $imagePath = $article['image_url'];


        if (!$article["visible"]) {
            continue;
        }

        $listing .= "<div class=\"post\">
        <h1 class=\"my-4\">$title</h1>

        <p class=\"lead\">
            <i class=\"fa fa-user\" aria-hidden=\"true\"></i> by $authorLogin
        </p>
        <hr>
        <p>
            <i class=\"fa fa-calendar\"></i> Posted on: $creationDate
        </p>
        <hr>
        <div class=\"card mb-4\">
            <img class=\"card-img-top\" src=\"$imagePath\" alt=\"img/notFound.png\">
            <div class=\"card-body\">
                <p class=\"card-text\">$description</p>
                <a href=\"index.php?action=article_view&id=$articleId\" class=\"btn btn-primary\">Read more</a>";

        if ($isAdmin) {
            $listing .= "
                <a href=\"index.php?action=edit_article&id=$articleId\" class=\"btn btn-warning\">Edit</a> 
                <button data-id=\"$articleId\" type=\"button\" class=\"btn btn-danger delete-article\">Delete</button>";
        }

        $listing .= "</div>
        </div>
    </div>";
    }

    echo $listing;

    ?>
</div>

<?php

include "layout/delete_article_modal.php";

function get_articles() {
    $dbConnection = get_db_connection();
    $result = $dbConnection->query("SELECT id, title, description, image_url, creation_date, visible, author_id FROM articles 
    ORDER BY creation_date DESC");

    return $result;
}

function get_author_login_by_id(&$error, $id) {
    $dbConnection = get_db_connection();
    $statement = $dbConnection->prepare("SELECT login FROM users WHERE id = ?");

    if ($statement && !$dbConnection->errno) {
        $statement->bind_param("i", $id);
        $statement->execute();
        $result = $statement->get_result();
        $loginData = $result->fetch_assoc();

        return $loginData['login'];
    }

    $error = "Something went wrong, please try again later";

    return null;
}

?>
<?php include "layout/footer.php"; ?>