<?php
if (array_key_exists('userId', $_SESSION)) {
    header('Location: index.php?action=main');
    die();
}
include "script/data_base.php";

$loginError = "";
$emailError = "";
$addressError = "";
$passwordError = "";
$repeatPasswordError = "";
$dbError = "";

$login = "";
$email = "";
$address = "";
$password = "";
$isFormValid = true;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    filter_post_input();

    if (!preg_match("/^[A-zА-я\-_\d]{4,20}$/", $_POST["login"])) {
        $isFormValid = false;
        $loginError = "Please enter valid login. Can contain A-zА-я_- at least 4 symbols";
    } else {
        $login = $_POST["login"];
    }

    if (!preg_match("/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$/", $_POST["email"])) {
        $isFormValid = false;
        $emailError = "Please enter valid email. Must contain @ and .";
    } else {
        $email = $_POST["email"];
    }
    if (!preg_match("/^(\p{Cyrillic}{2,6}\. [\p{Cyrillic} ?\-?]+, \d{1,3}((\\\d{1,3})|([А-яA-z]))?){0,255}$/u", $_POST["address"]) &&
        !preg_match("/^([A-z ?\-?]+ [A-z]{2,6}\., \d{1,3}((\\\d{1,3})|([A-z]))?){0,255}$/", $_POST["address"])) {
        $isFormValid = false;
        $addressError = "Please enter valid address, like вул. героїв Майдану, 4a or Geroiv Maidanu st., 1/11";
    } else {
        $address = $_POST["address"];
    }
    if (!preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{7,15}$/", $_POST["password"])) {
        $isFormValid = false;
        $passwordError = "Please enter valid password. Must contain one upper one lower case and digit at least 7 symbols";
    } else {
        $password = password_hash($_POST["password"], PASSWORD_BCRYPT);
    }
    if ($_POST["password"] != $_POST["repeatPassword"]) {
        $isFormValid = false;
        $repeatPasswordError = "Passwords must be equal";
    }

    if ($isFormValid
        && is_email_not_exist($email, $emailError, $dbError)
        & is_login_not_exist($login, $loginError, $dbError)) {

        register_user($login, $email, $address, $password, $dbError);
    }
}

function register_user($login, $email, $address, $password, &$dbError)
{
    $dbConnection = get_db_connection();
    $statement = $dbConnection->prepare("INSERT INTO users (login, email, address, password) VALUES (?, ?, ?, ?)");

    if ($statement) {
        $statement->bind_param("ssss", $login, $email, $address, $password);
        $statement->execute();
    } elseif ($dbConnection->errno || !$statement) {
        $dbError = "Something went wrong, please try again later";
        return;
    }

    header("Location: index.php?action=registration_successful");
}

function filter_post_input()
{
    foreach ($_POST as $key => $value) {
        $values[$key] = trim(stripslashes($value)); // basic input filter
    }
}

function is_email_not_exist($email, &$emailError, &$dbError)
{
    $dbConnection = get_db_connection();
    $statement = $dbConnection->prepare("SELECT COUNT(u.email) AS total FROM users u WHERE u.email = ?");

    if (!$statement) {
        $dbError = "Something went wrong, please try again later";
        return false;
    }

    $statement->bind_param("s", $email);
    $statement->execute();
    $result = $statement->get_result();

    if ($result->fetch_assoc()["total"] > 0) {
        $emailError = "Sorry, such email already exist";
        return false;
    }

    return true;
}

function is_login_not_exist($login, &$loginError, &$dbError)
{
    $dbConnection = get_db_connection();
    $statement = $dbConnection->prepare("SELECT COUNT(u.login) AS total FROM users u WHERE u.login = ?");

    if (!$statement) {
        $dbError = "Something went wrong, please try again later";
        return false;
    }

    $statement->bind_param("s", $login);
    $statement->execute();
    $result = $statement->get_result();

    if ($result->fetch_assoc()["total"] > 0) {
        $loginError = "Sorry, such login already exist";
        return false;
    }

    return true;
}

include "layout/registration_form.php";